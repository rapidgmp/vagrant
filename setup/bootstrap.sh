#!/usr/bin/env bash
###
# bootstrap.sh
# This script installs the server with app
###
# Variables
DBHOST="localhost"
DBNAME="gmp"
DBUSER="root"
DBPASSWD="rootPass"
ROOTDIR="/root/"
sudo apt update
echo "-----------------------------------------------Install MySQL"
sudo apt-get install debconf-utils -y
echo mysql-server mysql-server/root_password password ${DBPASSWD} | sudo debconf-set-selections
echo mysql-server mysql-server/root_password_again password ${DBPASSWD} | sudo debconf-set-selections
sudo apt-get install mysql-server -y
 mysql -uroot -p${DBPASSWD} -e "create database ${DBNAME}"
 mysql -uroot -p${DBPASSWD} -e "grant all privileges on *.* to ${DBUSER}@localhost identified by '${DBPASSWD}'"
 mysql -uroot -p${DBPASSWD} -e "FLUSH PRIVILEGES";
echo "--------------------------------------------Install ImageMagick"
sudo apt-get install -y imagemagick
echo "------------------------------------------Install nginx and php"
sudo apt-get install -y nginx
sudo apt-get install -y php-fpm php-json php-pdo php-mysql php-xml php-imagick php-mbstring php-curl php-zip
sudo cp /vagrant/provision/ubuntu/etc/nginx/sites-enabled/default /etc/nginx/sites-enabled/default
sudo cp -rf /vagrant/provision/ubuntu/etc/php/* /etc/php/
sudo service nginx restart
sudo systemctl restart php7.2-fpm.service
echo "--------------------------------------------Install Composer"
sudo apt-get install -y composer
sudo apt -y install zip
