#!/usr/bin/env bash

sudo apt-get update
sudo usermod -a -G vagrant www-data
echo "-----------------------------------Install Compass for scss"
sudo apt-get install -y build-essential dh-autoreconf > /dev/null 2>&1
sudo apt-get install -y ruby-dev  > /dev/null 2>&1
sudo gem install compass json
