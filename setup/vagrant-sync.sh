#!/bin/bash
lockfile="/tmp/sync.lock"
if [ -f "$lockfile" ]
then  exit 0
else
  # mkdir -p /home/vagrant/html
  touch $lockfile
  sudo rsync -avz /vagrant/provision/www/html /var/www/
  rm $lockfile
fi
