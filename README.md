# Rapid GMP Full Combo Repo

## Version (0.3.2.0)

This repo has the combination of the "front end" php hosted client interface pages and logic and the backend api.

The front end posts requests to the api address, which will later be moved.

# Instructions

## Versioning

The version is broken down into 4 points e.g 1.2.3.4 We use MAJOR.MINOR.FEATURE.PATCH to describe the version numbers.

A MAJOR is very rare, it would only be considered if the source was effectively re-written or a clean break was desired for other reasons. This increment would likely break most 3rd party modules.

A MINOR is when there are significant changes that affect core structures. This increment would likely break some 3rd party modules.

A FEATURE version is when new extensions or features are added (such as a payment gateway, shipping module etc). Updating a feature version is at a low risk of breaking 3rd party modules.

A PATCH version is when a fix is added, it should be considered safe to update patch versions e.g 1.2.3.4 to 1.2.3.5

## Installation

### Prerequisites
The repo is setup for virtualbox and vagrant. Make sure you have both programs installed before running the commands below

### Configure Vagrant
Duplicate the Vagrantfile.example file and rename it to just `Vagrantfile` in the repo's folder and make sure the ip address is in the same subnet as your local network.

### Configure Bootstrap
Duplicate the example bootstrap file located in `setup/bootstrap.example` and rename it to `bootstrap.sh`
The bootstrap file has some hard coded values. Update the variables at the top to match your settings.

### Install Frontend Vendor
Navigate to the api folder, located in `/var/www/html`
Run composer install in the html folder to install the vendor files. `composer install`
### Configure Frontend
Duplicate the `.env.example` file located in `provision/www/html/` and rename it to `.env`. Update the values in there to match your domain address in your `Vagrantfile`

### Configure Lumen
Duplicate the `.env.example` file located in `provision/www/html/api/` and rename it to `.env`. Update the values to match what you have setup in the previous bootstrap file.
in the JWT_SECRET section fill out with a list of characters, for example: `ab4f63f9ac65152575886860dde480a1`
This file is located at:`provision/www/html/api/.env.example`
In the end you should have the .env file `provision/www/html/api/.env`

### Startup Vagrant
Navigate to git folder in the terminal and type command below
`vagrant up`
If all goes well, you should be able to start using the program by navigating to the ip address specified in the Vagrantfile

The bootstrap file installed the required software and copies over updated config files to quickly start up a new server.

### Install Lumen Vendor
Navigate to the api folder, located in `/var/www/html/api`
Run composer install in the api folder to install the lumen vendor files. `composer install`


### Migrate Database
Login to the machine in the same location in the terminal.
`vagrant ssh`
Navigate to the api folder of the html files
`cd /var/www/html/api`
Run the phabricator migration
`php artisan migrate`
Run the db seed script, if you would like to start off with the demo content.
`php artisan db:seed`

## Test Connection
If you make rest get request to the root api location `http://192.168.0.114/api`
you should get this response
`Lumen (5.6.4) (Laravel Components 5.6.*)`

## Login to the software
Navigate to the ip address you setup in the Vagrantfile.
You can login with the username `master` and the password `123456`

## Troubleshooting

### Installation Troubleshooting

#### The bootstrap scripts did not execute correctly
Login and update the ubuntu repos.
`vagrant ssh`
`sudo apt update`
Manually run the bootstrap scripts and it should work the second time.
`sudo bash /vagrant/setup/bootstrap.sh`
The second script is optional for scss compiling using compass
`sudo bash /vagrant/setup/bootstrap-vagrant.sh`

#### Can't install plugins with composer
if the regular install doesn't complete. Try `composer install --prefer-source` instead

#### Migrate or Seeder file not found, but it exists and named correctly
This could be a permission issue. You won't be able to take advantage of the directory mounting vagrant does by default. You will just have to copy the files over, from Vagrant's default `/vagrant` directory so that Vagrant does not override the permissions on the folder.
Comment out the line that mounts the `/var/www/html` directory.
You will need to run this script once on bootup of your virtual machine.
`nohup watch -n 5 /vagrant/setup/vagrant-sync.sh > /dev/null &`
NOTE! this only goes one way. any updates made in `/var/www/html` will not reflect in your repo. You will need to stop the script above or reboot to copy the generated files over to your `/vagrant/provision/www/html` folder for the changes to be in your repo.

### Configuration Troubleshooting

#### Can't access api endpoint 403 access denied
The routing is handled through nginx. Check the nginx configuration settings in `/etc/nginx/sites-enabled/default`

#### Can't Login and Access Program But Token is Passed
Make sure the file located in `provision/www/html/data/db.class.php` has the same ip address as your `Vagrantfile` in the project root.

If that still doesn't work, make sure the .env file located in `provision/www/html/api/.env` has `JWT_SECRET` filled out.


## Authors

* **Tamar Zeithlian** - *Creator* - [rapidgmp](http://rapidgmp.com)

## License

This project is privately licensed by Tamar Zeithlian dba Rapid GMP
